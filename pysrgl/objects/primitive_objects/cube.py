from typing import Tuple
import numpy as np

def cube(center: Tuple[float, float, float], radius: float) -> Tuple[np.ndarray, np.ndarray]:
    g_vertex_buffer_data = np.array([
        0,0,0,
        0,0, radius,
        0, radius, radius,
         radius, radius,0,
        0,0,0,
        0, radius,0,
         radius,0, radius,
        0,0,0,
         radius,0,0,
         radius, radius,0,
         radius,0,0,
        0,0,0,
        0,0,0,
        0, radius, radius,
        0, radius,0,
         radius,0, radius,
        0,0, radius,
        0,0,0,
        0, radius, radius,
        0,0, radius,
         radius,0, radius,
         radius, radius, radius,
         radius,0,0,
         radius, radius,0,
         radius,0,0,
         radius, radius, radius,
         radius,0, radius,
         radius, radius, radius,
         radius, radius,0,
        0, radius,0,
         radius, radius, radius,
        0, radius,0,
        0, radius, radius,
         radius, radius, radius,
        0, radius, radius,
         radius, 0, radius,
    ], dtype=np.float32).reshape(-1, 3) + center
    return {"vertex": g_vertex_buffer_data}

def cube_ibo(center: Tuple[float, float, float], radius: float) -> Tuple[np.ndarray, np.ndarray]:
    vertices = np.float32([
        (center[0] - radius, center[1] - radius, center[2] + radius),
        (center[0] + radius, center[1] - radius, center[2] + radius),
        (center[0] + radius, center[1] + radius, center[2] + radius),
        (center[0] - radius, center[1] + radius, center[2] + radius),
        (center[0] - radius, center[1] - radius, center[2] - radius),
        (center[0] + radius, center[1] - radius, center[2] - radius),
        (center[0] + radius, center[1] + radius, center[2] - radius),
        (center[0] - radius, center[1] + radius, center[2] - radius)
    ])
    indices = np.int32([
        (0, 1, 2),
        (2, 3, 0),
        (1, 5, 6),
        (6, 2, 1),
        (7, 6, 5),
        (5, 4, 7),
        (4, 0, 3),
        (3, 7, 4),
        (4, 5, 1),
        (1, 0, 4),
        (3, 2, 6),
        (6, 7, 3),
    ])
    return {"vertex": vertices, "index": indices}
