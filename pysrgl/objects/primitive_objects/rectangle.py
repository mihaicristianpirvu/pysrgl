from typing import Tuple
import numpy as np

def rectangle(topLeft: Tuple[float, float], bottomRight: Tuple[float, float]) -> Tuple[np.ndarray, np.ndarray]:
    assert topLeft[0] <= bottomRight[0] and topLeft[1] <= bottomRight[1]
    dx = bottomRight[0] - topLeft[0]
    dy = bottomRight[1] - topLeft[1]
    vertices = np.float32([
        (topLeft[0], topLeft[1], 0),
        (topLeft[0], bottomRight[1], 0),
        (bottomRight[0], bottomRight[1], 0),
        (bottomRight[0], topLeft[1], 0),
    ])

    indices = np.int32([
        (0, 1, 2),
        (2, 3, 0)
    ])
    return {"vertex": vertices, "index": indices}

