import numpy as np
from typing import Tuple

def circle(center: Tuple[float, float], radius: float, numSegments: int=20) -> Tuple[np.ndarray, np.ndarray]:
    center = (center[0], center[1], 0)
    vertices = []
    def f(i, numSegments, center):
        theta = 2 * np.pi * (i % numSegments) / numSegments
        x = radius * np.cos(theta)
        y = radius * np.sin(theta)
        return x + center[0], y + center[1], 0

    for i in range(numSegments):
        current = f(i, numSegments, center)
        next = f(i +1, numSegments, center)
        vertices.extend([center, current, next])
    return {"vertex": vertices}

def circle_ibo(center: Tuple[float, float], radius: float, numSegments: int=20) -> Tuple[np.ndarray, np.ndarray]:
    vertices = [(center[0], center[1], 0)]
    for i in range(numSegments):
        theta = 2 * np.pi * i / numSegments
        x = radius * np.cos(theta)
        y = radius * np.sin(theta)
        vertices.append((x + center[0], y + center[1], 0))
    indices = []
    for i in range(numSegments):
        indices.append([0, i + 1, i + 2])
    indices[-1] = (0, numSegments, 1)
    
    vertices = np.array(vertices)
    indices = np.array(indices)
    return {"vertex": vertices, "index": indices}
