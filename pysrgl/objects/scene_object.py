from abc import abstractmethod, ABC
from typing import Dict
import numpy as np
from OpenGL.arrays import vbo
from OpenGL.GL import GL_ELEMENT_ARRAY_BUFFER
from ..utils.logger import logger

Scene = int

class SceneObject(ABC):
    def __init__(self, buffers: Dict[str, np.ndarray], shaderName: str):
        # Store the vertices and indices, then build opengl VBO and IBO from them
        self.buffers = self._buildBuffers(buffers)
        self.shaderName = shaderName

    def _buildBuffers(self, buffers: Dict[str, np.ndarray]) -> Dict[str, vbo.VBO]:
        logger.debug(f"Creating OpenGL buffers (keys: {list(buffers.keys())}).")
        assert "vertex" in buffers
        if not "index" in buffers:
            logger.debug2("Index buffer not provided. Creating manually.")
            assert len(buffers["vertex"]) % 3 == 0, \
                    "If no indexes are provided, vertex buffer must be divisible by 3 to create triangles"
            buffers["index"] = np.arange(len(buffers["vertex"])).reshape(-1, 3)
        else:
            logger.debug2("Index buffer provided.")

        res = {}
        for key in buffers:
            if key == "index":
                continue
            res[key] = vbo.VBO(np.float32(buffers["vertex"]))
        res["index"] = vbo.VBO(np.array(buffers["index"], dtype=np.int32), target=GL_ELEMENT_ARRAY_BUFFER)
        return res

    def _bindBuffers(self):
        pass

    @abstractmethod
    def initialize(self, scene: Scene):
        """Called at scene initialization or when objects are added."""
        pass

    @abstractmethod
    def draw(self, scene: Scene):
        """Called on each frame."""
        pass

    def getNumVertices(self) -> int:
        return len(self.buffers["vertex"].flatten())

    def getNumIndices(self) -> int:
        return len(self.buffers["index"].flatten())
