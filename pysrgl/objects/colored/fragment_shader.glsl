#version 330 core

in vec3 VS_Color;
out vec4 FS_Color;

void main()
{
    FS_Color = vec4(VS_Color, 1.0);
}
