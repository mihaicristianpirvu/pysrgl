from typing import Tuple, Dict, Optional, Callable, List, Union
from pathlib import Path
import glfw
import numpy as np

from OpenGL.GL import *
from OpenGL.GLU import gluLookAt, gluPerspective

from .camera import Camera
from .utils.textures import loadTexture
from .utils.logger import logger
from .utils.shader import Shader, getDefaultShaders

ShaderType = Union[Shader, Tuple[str, str], Tuple[Path, Path]] # VertexShader, FragmentShader
ShaderProgram = int
SceneObject = int

class Scene:
    def __init__(self, windowSize: Tuple[int, int], polygons: Dict = {}, textures: Dict[str, Path] = {},
            shaders: Dict[str, ShaderType] = {}, cameras: List[Camera] = [], callback: Optional[Callable] = None, \
            callbackInitializer: Optional[Callable] = None, targetFps: int = 60):
        if callback is None:
            callback = lambda _: None
        if callbackInitializer is None:
            callbackInitializer = lambda _: None
        if len(cameras) == 0:
            cameras.append(Camera())

        self.windowSize = windowSize
        self.width, self.height = windowSize
        self._activeShader = None
        self._openGLInitializer(polygons, shaders, textures)

        # Private attributes
        self.projectionMatrix = np.array([
            [2, 0, 0, 0],
            [0, -2, 0, 0],
            [0, 0, -1, 0],
            [-1, 1, 0, 1]
        ])
        self.cameras = cameras
        self.activeCamera = cameras[0]
        # Set the callback for scene logic.
        self.callback = callback
        self.callbackInitializer = callbackInitializer

        self.numUpdatesSinceEpoch = 0
        self.targetFps = targetFps
        self.fps = targetFps

    def useShaderProgram(self, shaderName: str):
        if self._activeShader != self.shaders[shaderName].shaderProgram:
            logger.debug2(f"Setting the shader to: '{shaderName}'")
            self._activeShader = self.shaders[shaderName].shaderProgram
            glUseProgram(self._activeShader)

    def _openGLInitializer(self, polygons: Dict[str, SceneObject],
            shaders: Dict[str, ShaderType], textures: Dict[str, Path]):
        glfw.init()
        self.window = glfw.create_window(self.windowSize[0], self.windowSize[1], "", None, None)
        glfw.make_context_current(self.window)

        glClearColor(0.0,0,0.4,0)
        glDepthFunc(GL_LESS)
        glEnable(GL_DEPTH_TEST)
        # glEnable(GL_CULL_FACE)

        class NullShader:
            def __init__(self):
                self.shaderProgram = 0
        self.shaders = {"null": NullShader()}
        defaultShaders = getDefaultShaders()
        assert len(set(shaders.keys()).intersection(defaultShaders.keys())) == 0, f"One or more default shader " + \
          f"names are in provided shaders: {shaders.keys()} vs {defaultShaders.keys()}"
        shaders = {**shaders, **defaultShaders}
        for name, shader in shaders.items():
            self.addShader(name, shader)

        self.textures = {}
        for name, texture in textures.items():
            self.addTexture(name, texture)

        self.polygons = {}
        self.polygonInitialized = {}
        for name, polygon in polygons.items():
            self.addObject(name, polygon)

    # Gets all the drawable objects for a specific frame. Defaults to all.
    def getDrawableObjects(self) -> List:
        return self.polygons

    def screenShot(self, x: Optional[float] = 0, y : Optional[float] = 0) -> np.ndarray:
        glReadBuffer(GL_FRONT)
        rawPixels = glReadPixels(x, y, self.width, self.height, GL_RGB, GL_FLOAT, None)
        glFlush()
        image = np.frombuffer(rawPixels, np.float32)
        image.shape = (self.height, self.width, 3)
        image = image[::-1, :]
        image = (image * 255).astype(np.uint8)
        return image

    def addShader(self, name: str, shader: ShaderType):
        assert name not in self.shaders, f"Shader {name} already in shaders list: {self.shaders.keys()}"
        if not isinstance(shader, Shader):
            shader = Shader(shader)
        self.shaders[name] = shader
        logger.debug(f"Adding shader '{name}' (ID: {shader.shaderProgram}).")

    def removeShader(self, name: str):
        logger.debug(f"Removing shader '{name}'.")
        assert name in self.shaders, f"Shader {name} not in shaders list: {self.shaders.keys()}"
        del self.shaders[name]

    def addTexture(self, name: str, path: Path):
        logger.debug(f"Adding texture '{name}'.")
        assert name not in self.textures, f"Shader {name} already in shaders list: {self.textures.keys()}"
        self.textures[name] = loadTexture(path)

    def addObject(self, name: str, obj):
        assert not name in self.polygons.keys(), f"Object {name} already exists."
        Type = str(type(obj)).split(".")[-1][0:-2]
        logger.debug(f"Adding and initializing object '{name}'. Type: '{Type}'. Shader: '{obj.shaderName}'.")
        self.polygons[name] = obj
        self.polygonInitialized[name] = False
        self.polygons[name].initialize(self)

    def removeObject(self, name: str):
        logger.debug(f"Removing object '{name}'.")
        assert name in self.polygons.keys(), f"Object {name} doesn't exist."
        del self.polygons[name]
        del self.polygonInitialized[name]

    def run(self):
        """Main function to be called to start the scene and call the scene logic callback."""
        self.numUpdatesThisSecond = 0
        # Desired delta (for fps=20, we want delta=1/20, so each update at 0.05 seconds)
        targetDelta = 1 / self.fps
        self.callbackInitializer(self)

        lastTime = glfw.get_time()
        while True:
            currentTime = glfw.get_time()
            self.useShaderProgram("null")
            # glEnableVertexAttribArray(0)
            glfw.poll_events()
            if glfw.window_should_close(self.window):
                break
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
            delta = currentTime - lastTime
            # Just draw the existing things if delta is too small
            if delta < targetDelta:
                glDrawArrays(GL_TRIANGLES, 0, 3)
                continue

            self.numUpdatesThisSecond += 1
            self.numUpdatesSinceEpoch += 1
            if int(currentTime) != int(lastTime):
                self.fps = (self.fps * 9 + self.numUpdatesThisSecond) / 10
                self.numUpdatesThisSecond = 0

            self.callback(self)

            for name, polygon in self.getDrawableObjects().items():
                logger.debug2(f"Drawing object '{name}'.")
                self.useShaderProgram(polygon.shaderName)
                polygon.draw(self)
                glDrawElements(GL_TRIANGLES, polygon.getNumVertices(), GL_UNSIGNED_INT, None)

            lastTime = currentTime
            glfw.swap_buffers(self.window)
        glfw.terminate()

    def __call__(self):
        return self.run()

    def __str__(self) -> str:
        numVertices, numIndices = 0, 0
        for polygon in self.polygons.values():
            numVertices += polygon.getNumVertices()
            numIndices += polygon.getNumIndices()

        Str = "[SceneRenderer]"
        Str += f"\n - Window size: {self.height}x{self.width}"
        Str += f"\n - Num polygons: {len(self.polygons)}"
        Str += f"\n - Num vertices: {numVertices}"
        Str += f"\n - Num indices: {numIndices}"
        Str += f"\n - Num shaders: {len(self.shaders)}"
        Str += f"\n - Num cameras: {len(self.cameras)}"
        return Str
