import numpy as np
from OpenGL.GL import GL_LINE, GL_FILL, GL_FRONT_AND_BACK, glPolygonMode
from pysrgl import Scene
from pysrgl.objects import ColoredObject, OneColorObject, TexturedObject, Text, FPSText
from pysrgl.objects.mvp.mvp_object import MVPObject
from pysrgl.utils.keyboard import keyPress, keyPressAndRelease
from pysrgl.objects.primitive_objects import cube, triangle, rectangle, verticalLine
from pathlib import Path
from media_processing_lib.image import tryWriteImage

def initializer(scene):
    scene.wireframe = False

# Called on each frame for all objects.
def callback(scene):
    if keyPress(scene, "ESC"):
        exit()
    if keyPress(scene, "1"):
        scene.polygons["cube"].resetPosition()
    if keyPressAndRelease(scene, "I"):
        scene.wireframe = not scene.wireframe
        value = GL_LINE if scene.wireframe == True else GL_FILL
        glPolygonMode(GL_FRONT_AND_BACK, value)
    if keyPress(scene, "RIGHT"):
        scene.polygons["triangle"].translate(0.01, 0, 0)
    if keyPress(scene, "LEFT"):	
        scene.polygons["triangle"].translate(-0.01, 0, 0)
    if keyPress(scene, "UP"):
        scene.polygons["triangle"].translate(0, -0.01, 0)
    if keyPress(scene, "DOWN"):
        scene.polygons["triangle"].translate(0, 0.01, 0)
    if keyPressAndRelease(scene, "P"):
        print(f"Screenshot took. Saved to screenshot_{scene.numUpdatesSinceEpoch}.png")
        tryWriteImage(scene.screenShot(), f"screenshot_{scene.numUpdatesSinceEpoch}.png")
    if keyPress(scene, "D"):
        scene.activeCamera.translate((0.05, 0, 0))
    if keyPress(scene, "A"):
        scene.activeCamera.translate((-0.05, 0, 0))
    if keyPress(scene, "W"):
        scene.activeCamera.translate((0, -0.05, 0))
    if keyPress(scene, "S"):
        scene.activeCamera.translate((0, 0.05, 0))
    if keyPress(scene, "R"):
        scene.activeCamera.reset()

    # Update angle every frame
    if keyPress(scene, "X"):
        scene.polygons["cube"].rotate(0, 0.01, 0)
    if keyPress(scene, "C"):
        scene.polygons["cube"].rotate(0, -0.01, 0)

def main():
    uv = np.array([
        0.000059, 1.0-0.000004,
        0.000103, 1.0-0.336048,
        0.335973, 1.0-0.335903,
        1.000023, 1.0-0.000013,
        0.667979, 1.0-0.335851,
        0.999958, 1.0-0.336064,
        0.667979, 1.0-0.335851,
        0.336024, 1.0-0.671877,
        0.667969, 1.0-0.671889,
        1.000023, 1.0-0.000013,
        0.668104, 1.0-0.000013,
        0.667979, 1.0-0.335851,
        0.000059, 1.0-0.000004,
        0.335973, 1.0-0.335903,
        0.336098, 1.0-0.000071,
        0.667979, 1.0-0.335851,
        0.335973, 1.0-0.335903,
        0.336024, 1.0-0.671877,
        1.000004, 1.0-0.671847,
        0.999958, 1.0-0.336064,
        0.667979, 1.0-0.335851,
        0.668104, 1.0-0.000013,
        0.335973, 1.0-0.335903,
        0.667979, 1.0-0.335851,
        0.335973, 1.0-0.335903,
        0.668104, 1.0-0.000013,
        0.336098, 1.0-0.000071,
        0.000103, 1.0-0.336048,
        0.000004, 1.0-0.671870,
        0.336024, 1.0-0.671877,
        0.000103, 1.0-0.336048,
        0.336024, 1.0-0.671877,
        0.335973, 1.0-0.335903,
        0.667969, 1.0-0.671889,
        1.000004, 1.0-0.671847,
        0.667979, 1.0-0.335851
    ], dtype=np.float32)


    polygons = {
        "triangle" : OneColorObject(triangle(center=(0.5, 0.5), radius=0.05), color=(255, 0, 0)),
        "rectangle" : OneColorObject(rectangle(topLeft=(0.4, 0.4), bottomRight=(0.6, 0.6)), color=(255, 255, 0)),
        "cube" : TexturedObject(cube(center=(0.25, 0.25, 0.25), radius=0.1), textureUV=uv, textureName="cube"),
        # "cube" : MVPObject(cube(center=(0.25, 0.25, 0.25), radius=0.1)),
        "line" : OneColorObject(verticalLine(startPosition=(0.1, 0.1), length=0.1), color=(255, 0, 0)),
        "line2" : OneColorObject(verticalLine(startPosition=(0.7, 0.1), length=0.2), color=(255, 0, 0)),
        "text": Text("Titlu spumos", position=(0.2, 0.2), scale=0.001, color=(255, 0, 255)),
        "fps": FPSText(position=(0, 0.05), scale=0.001, color=(0, 0, 255))
    }

    viewer = Scene(windowSize = (640, 480), polygons = polygons, \
        callback = callback, callbackInitializer = initializer, \
        textures={"cube": Path(__file__).absolute().parent / "uvtemplate.bmp"})
    print(viewer)
    viewer.run()

if __name__ == "__main__":
    main()
